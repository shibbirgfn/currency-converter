package com.example.torik.currencyconverter;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class CustomExchangeSetupActivity extends AppCompatActivity {

    EditText firstCurrencyName, secondCurrencyName, exchangeRate;
    Button createNewExchange;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_exchange_setup);

        firstCurrencyName = (EditText)findViewById(R.id.et_activity_custom_exchange_setup_first_currency_name);
        secondCurrencyName = (EditText)findViewById(R.id.et_activity_custom_exchange_setup_second_currency_name);
        exchangeRate = (EditText)findViewById(R.id.et_activity_custom_exchange_setup_exchange_rate);

        createNewExchange = (Button)findViewById(R.id.bt_activity_custom_exchange_setup_create_new_exchange);

        createNewExchange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String currentFirstCurrencyName = firstCurrencyName.getText().toString();
                //first currency name validation
                if(currentFirstCurrencyName.length()>0){
                    String currentSecondCurrencyName = secondCurrencyName.getText().toString();
                    //second currency name validation
                    if (currentSecondCurrencyName.length()>0){
                        //exchange rate validation
                        String currentExchangeRate = exchangeRate.getText().toString();

                        if (currentExchangeRate.length()>0){
                            double currentExchange = Double.parseDouble(currentExchangeRate);
                            if(currentExchange > 0){

                                Intent intent = new Intent(getApplicationContext(),CustomExchangeActivity.class);
                                intent.putExtra("firstCurrencyName", currentFirstCurrencyName);
                                intent.putExtra("secondCurrencyName", currentSecondCurrencyName);
                                intent.putExtra("exchangeRate", currentExchange);
                                startActivity(intent);

                            }else{
                                showToast("Enter valid exchange value");
                            }
                        }else{
                            showToast("Enter exchange rate");
                        }
                    }else{
                        showToast("Enter second currency name");
                    }
                }else{
                    showToast("Enter first currency name");
                }
            }
        });


    }

    private void showToast(String string){
        Toast.makeText(getApplicationContext(),string, Toast.LENGTH_SHORT).show();
    }
}
