package com.example.torik.currencyconverter;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class CustomExchangeActivity extends AppCompatActivity {

    EditText exchangeRate, input;
    RadioGroup currencyNames;
    RadioButton firstCurrencyToSecond, secondCurrencyTofirst;
    Button convert;
    TextView showResult;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_exchange);

        exchangeRate = (EditText)findViewById(R.id.et_activity_custom_exchange_exchange_rate);
        input = (EditText)findViewById(R.id.et_activity_custom_exchange_input);

        currencyNames = (RadioGroup)findViewById(R.id.rg_activity_custom_exchange_currencies);
        firstCurrencyToSecond = (RadioButton)findViewById(R.id.rb_activity_custom_exchange_first_currency_to_second_currency);
        secondCurrencyTofirst = (RadioButton)findViewById(R.id.rb_activity_custom_exchange_second_currency_to_first_currency);

        convert = (Button)findViewById(R.id.bt_activity_custom_exchange_convert);
        showResult = (TextView)findViewById(R.id.tv_activity_custom_exchange_show_result);

        //catch passed value;
        String firstCurrencyName = getIntent().getExtras().getString("firstCurrencyName");
        String secondCurrencyName = getIntent().getExtras().getString("secondCurrencyName");
        double passedExchangeRate = getIntent().getExtras().getDouble("exchangeRate");

        //assign passed value in view
        exchangeRate.setText(String.valueOf(passedExchangeRate));
        String firstRadioButtonText = firstCurrencyName+" to "+secondCurrencyName;
        String secondRadioButtonText = secondCurrencyName+" to "+firstCurrencyName;

        firstCurrencyToSecond.setText(firstRadioButtonText);
        secondCurrencyTofirst.setText(secondRadioButtonText);

        convert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //get current exchange rate
                String currentExchangeRate = exchangeRate.getText().toString();
                //current exchange empty validation
                if(currentExchangeRate.length()>0){
                    //convert exchange rate string to double;
                    double exchangeRate = Double.parseDouble(currentExchangeRate);
                    //get input value
                    String screen = input.getText().toString();
                    if(screen.length()>0){
                        double inputValue = Double.parseDouble(screen);

                        //get checked radio button
                        int checkedRadioButtonId = currencyNames.getCheckedRadioButtonId();
                        //radio button empty validation

                        if(checkedRadioButtonId != -1){
                            RadioButton checkedRadioButton = (RadioButton)findViewById(checkedRadioButtonId);

                            if(checkedRadioButton.getId() == firstCurrencyToSecond.getId()){
                                double result = inputValue * exchangeRate;
                                showResult.setText(String.valueOf(result));
                            }
                            if(checkedRadioButton.getId() == secondCurrencyTofirst.getId()){
                                double result = inputValue / exchangeRate;
                                showResult.setText(String.valueOf(result));
                            }
                        }else{
                            showToast("Select exchange option");
                        }



                    }else{
                        showToast("Input value");
                    }
                }else{
                    showToast("Input valid exchange rate");
                }
            }
        });

    }

    private void showToast(String text){
        Toast.makeText(getApplicationContext(),text,Toast.LENGTH_SHORT).show();
    }
}
