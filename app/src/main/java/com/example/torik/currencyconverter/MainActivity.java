package com.example.torik.currencyconverter;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button usdBdt;
    Button customExchange;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        usdBdt = (Button)findViewById(R.id.bt_activity_main_usd_bdt);
        customExchange = (Button)findViewById(R.id.bt_activity_main_custom);

        usdBdt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent  = new Intent(getApplicationContext(), UsdBdtActivity.class);
                startActivity(intent);
            }
        });

        customExchange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), CustomExchangeSetupActivity.class);
                startActivity(intent);
            }
        });

    }
}
