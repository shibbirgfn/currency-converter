package com.example.torik.currencyconverter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class UsdBdtActivity extends AppCompatActivity {

    EditText input;
    Button convert;
    RadioGroup currncies;
    RadioButton usdToBdt;
    RadioButton bdtToUsd;
    TextView result;
    int selectedRb;
    double var1;

    String screen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usd_bdt);

        input = (EditText)findViewById(R.id.et_activity_usd_bdt_input_field);

        convert = (Button)findViewById(R.id.bt_activity_usd_bdt_convert);

        currncies = (RadioGroup)findViewById(R.id.rg_activity_usd_bdt_currencies_group);
        usdToBdt = (RadioButton)findViewById(R.id.rb_activity_usd_bdt_usd_to_bdt);
        bdtToUsd = (RadioButton)findViewById(R.id.rb2_activity_usd_bdt_bdt_to_usd);

        result = (TextView)findViewById(R.id.tv_activity_usd_bdt_result);



        convert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                screen = input.getText().toString();

                if (screen.length()>0){

                    var1=Double.parseDouble(screen);
                    selectedRb=currncies.getCheckedRadioButtonId();

                    if(selectedRb == -1){
                        Toast.makeText(getApplicationContext(), "Select at least one option",Toast.LENGTH_SHORT).show();
                    }else{

                        RadioButton selectedRadioButton = (RadioButton)findViewById(selectedRb);

                        if(selectedRadioButton.getId() == usdToBdt.getId()){
                            var1 = var1*80.0;
                        }else if (selectedRadioButton.getId() == bdtToUsd.getId()){
                            var1 = var1/80.0;
                        }
                        result.setText(String.valueOf(var1));
                    }
                }else{
                    Toast.makeText(getApplicationContext(),"Input value in the text box", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
